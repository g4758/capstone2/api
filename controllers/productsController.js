
const Product = require('./../models/Products');

//CREATE A PRODUCT
module.exports.create = async (reqBody) => {
	const {productName, description, price} = reqBody

	let newProduct = new Product({
		productName: productName,
		description: description,
		price: price
	})

	return await newProduct.save().then((result, err) => result ? result : err)
}

//GET ALL PRODUCTS
module.exports.getAllProducts = async () => {
	return await Product.find().then(result => result)
}

//In Stock products
module.exports.inStock = async () => {

	return await Product.find({isOffered:true}).then(result => result)
}

//GET SPECIFIC Product
module.exports.getAProduct = async (id) => {

	return await Product.findById(id).then((result, err) => {
		if(result){
			return result
		} else {
			if(result == null){
				return {message: `Product not found`}
			}else{
				return err
			}
		}
	})
}

//UPDATE A PRODUCT
module.exports.updateProduct = async (productId, reqBody) => {

	return await Product.findByIdAndUpdate(productId, {$set: reqBody}, {new:true}).then(result => result)
}
