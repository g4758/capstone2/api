const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, `First name is required`],
		unique: true
	},
	Description: {
		type: String,
		required: [true, `Description is required`]
	},
    price: {
        type: Number,
        required: [true, `Price is required`] 
    },


	isActive: {
		type: Boolean,
		default: true
	},

}, {timestamps: true})

//Export the model
module.exports = mongoose.model(`Products`, userSchema);