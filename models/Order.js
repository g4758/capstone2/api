const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    totalAmount: {
        type: Number,
        required: [true, `Price is required`] 
    },

	userId: {
		type: String,
        required: [true, `user Id is required`]

	},

    productIds: {
        type: Array,
        of: String
    }

}, {timestamps: true})

//Export the model
module.exports = mongoose.model(`Order`, userSchema);