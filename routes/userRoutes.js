const express = require('express');
const router = express.Router();

// Import units of functions from userController module
const {
	register,
	getAllUsers,
	checkEmail,
	login,
	profile,
	update,
	updatePw,
	adminStatus,
	userStatus,
	deleteUser,
	getUserOrder
	
} = require('../controllers/userController');

const {verify, decode, verifyAdmin} = require('./../auth');

//GET ALL USERS
router.get('/', async (req, res) => {

	try{
		await getAllUsers().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

//REGISTER USER 
router.post('/register', async (req, res) => {

	try{
		await register(req.body).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})

//CHECK IF EMAIL EXISTS
router.post('/email-exists', async (req, res) => {
	try{
		await checkEmail(req.body).then(result => res.send(result))

	}catch(error){
		res.status(500).json(error)
	}
})


//LOGIN THE USER
	// authentication
router.post('/login', (req, res) => {try{login(req.body).then(result => res.send(result))}catch(err){res.status(500).json(err)}
})

//RETRIEVE USER INFORMATION
router.get('/profile', verify, async (req, res) => {
	const userId = decode(req.headers.authorization).id

	try{
		profile(userId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

router.put('/updateUser', verify, async (req, res) => {

	const userId = decode(req.headers.authorization).id
	try{
		await update(userId, req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

router.patch('/update-password', verify, async (req, res) => {
	console.log( decode(req.headers.authorization).id )
	console.log(req.body.password)

	const userId = decode(req.headers.authorization).id
	try{
		await updatePw(userId, req.body.password).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})

router.patch('/isAdmin', verifyAdmin, async (req, res) => {
	try{
		await adminStatus(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})

router.patch('/isUser', verifyAdmin, async (req, res) => {
	try{
		await userStatus(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

router.delete('/delete-user', verifyAdmin, async (req, res) => {
	try{
		deleteUser(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

router.get(`/test`, verify, async (req, res) => {
	try{
	  getUserOrder().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

module.exports = router;