const express = require('express')
const router = express.Router()

const {
	selectProduct, 
	getAllProducts,
	addProduct,
	updateProduct,
	inStock,
	outOfStock

} = require('../controllers/productsController')

const {verifyAdmin, verify} = require('./../auth')

router.post('/selectProduct', verifyAdmin, async (req, res) => {
	try{
		selectProduct(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//"/" to get all the Products, return all documents found
router.get('/', verify, async (req, res) => {

	try{
		await getAllProducts().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})


//  "/inStock" route to get all in stock items, return all documents found
router.get('/inStock', verify, async (req, res) => {
	try{
		await inStock().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

//Create a route "/:productId" to retrieve a specific product, save the product in DB and return the document
	//both admin and regular user can access this route
router.get('/:productId', verify, async (req, res) => {
	try{
		await addProduct(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

//Create a route "/:productId/update" to update product info, return the updated document
router.put('/:productId/update', verifyAdmin, async (req, res) => {
	try{
		await updateProduct(req.params.productId, req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

//Create a route "/:productId/out-of-stock" to show items out of stock, return true if success
router.delete('/:productId/out-of-stock', verifyAdmin, async (req, res) => {
	try{
		await outOfStock(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


module.exports = router