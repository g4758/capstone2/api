const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const cors = require('cors')

const PORT = process.env.PORT || 4007;
const app = express();

// Connect routes module
const userRoutes = require('./routes/userRoutes');
const productsRoutes = require('./routes/productsRoutes')


//Middleware to handle JSON payloads
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())

mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to Database`));

// Routes

app.use(`/api/users`, userRoutes);
app.use(`/api/products`, productsRoutes);



app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))